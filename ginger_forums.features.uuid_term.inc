<?php
/**
 * @file
 * ginger_forums.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function ginger_forums_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Other Pets',
    'description' => '',
    'format' => NULL,
    'weight' => 3,
    'uuid' => '1cd4c92b-0413-4cc8-a5cb-ff436a3151c1',
    'vocabulary_machine_name' => 'forums',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'All about Cats',
    'description' => '',
    'format' => NULL,
    'weight' => 2,
    'uuid' => '79677df5-0bbf-4b01-8ee6-f3241d3afc44',
    'vocabulary_machine_name' => 'forums',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Forum administration and code of conduct',
    'description' => 'All the post related to managing this forum',
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a5858ef3-f82e-4ccb-bd58-d8ecfa3e9e91',
    'vocabulary_machine_name' => 'forums',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'All about Dogs',
    'description' => '',
    'format' => NULL,
    'weight' => 3,
    'uuid' => 'dfdc02b5-78f9-4a09-9f76-8421b08ba68e',
    'vocabulary_machine_name' => 'forums',
    'metatags' => array(),
  );
  return $terms;
}
