<?php
/**
 * @file
 * ginger_forums.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ginger_forums_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
