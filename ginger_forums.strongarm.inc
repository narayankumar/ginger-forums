<?php
/**
 * @file
 * ginger_forums.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ginger_forums_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'forum_containers';
  $strongarm->value = array(
    0 => '34',
    1 => '35',
    2 => '38',
  );
  $export['forum_containers'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'forum_nav_vocabulary';
  $strongarm->value = '7';
  $export['forum_nav_vocabulary'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_forum_pattern';
  $strongarm->value = '[term:vocabulary]/[term:name]';
  $export['pathauto_forum_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_forum_pattern';
  $strongarm->value = 'forums/[node:taxonomy_forums]/[node:title]';
  $export['pathauto_node_forum_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_dim_active';
  $strongarm->value = '0';
  $export['vud_comment_dim_active'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_dim_threshold';
  $strongarm->value = '0';
  $export['vud_comment_dim_threshold'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_node_types';
  $strongarm->value = array(
    'forum' => 'forum',
    'advertisement' => 0,
    'announcement' => 0,
    'article' => 0,
    'page' => 0,
    'directory_entry' => 0,
    'guest_column' => 0,
    'found_pet' => 0,
    'lost_pet' => 0,
    'news' => 0,
    'pet_for_adoption' => 0,
    'pet_video' => 0,
    'node_gallery_item' => 0,
    'node_gallery_gallery' => 0,
    'photo_contest' => 0,
    'poll' => 0,
    'video_contest_entry' => 0,
    'photo_contest_entry' => 0,
    'video_contest' => 0,
  );
  $export['vud_comment_node_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_reset';
  $strongarm->value = '0';
  $export['vud_comment_reset'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_votes';
  $strongarm->value = '1';
  $export['vud_comment_votes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_widget';
  $strongarm->value = 'updown';
  $export['vud_comment_widget'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_comment_widget_display';
  $strongarm->value = '1';
  $export['vud_comment_widget_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_message_on_deny';
  $strongarm->value = 0;
  $export['vud_message_on_deny'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'vud_tag';
  $strongarm->value = 'updownvote';
  $export['vud_tag'] = $strongarm;

  return $export;
}
